package fr.dawan.formationjpa.listeners;

import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import fr.dawan.formationjpa.entities.Article;

public class CustomListener {

    @PreUpdate
    public void onPreUpdate(Article a) {
        System.out.println("Pre update Article" + a);
    }
    
    @PrePersist
    public void onPrePersist(Article a) {
        System.out.println("Pre persit Article" + a);
    }
    
     @PostLoad
     public void onPostLoad(Article a) {
         System.out.println("Post Load Article" + a);
     }
    
}
