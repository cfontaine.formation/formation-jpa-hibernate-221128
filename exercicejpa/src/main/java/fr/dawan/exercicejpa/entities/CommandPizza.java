package fr.dawan.exercicejpa.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "commande_pizza")
public class CommandPizza implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    private CommandePizzaId id;

    @ManyToOne
    @MapsId("commandeId")
    private Commande commande;
    
    @ManyToOne
    @MapsId("pizzaId")
    private Pizza pizza;
    
    private int quantite;

    public CommandPizza() {
       
    }

    public CommandPizza(Commande commande, Pizza pizza, int quantite) {
        super();
        this.commande = commande;
        this.pizza = pizza;
        this.quantite = quantite;
    }

    public CommandePizzaId getId() {
        return id;
    }

    public void setId(CommandePizzaId id) {
        this.id = id;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
    
    
    
    
}
