package fr.dawan.formationjpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.Maire;
import fr.dawan.formationjpa.entities.Ville;

public class Main04OneToOne 
{
    public static void main( String[] args )
    {
       EntityManagerFactory emf=Persistence.createEntityManagerFactory("formationjpa");
       EntityManager em=emf.createEntityManager();
       EntityTransaction tx=em.getTransaction();
       
       Maire maire=new Maire("Martine");
       Ville lille=new Ville("Lille");
       maire.setVille(lille);
       lille.setMaire(maire);
       System.out.println(maire.getVille());
       try {
           tx.begin();
           em.persist(lille);
           em.persist(maire);
//            Maire martine=em.find(Maire.class,1L);
//            System.out.println(martine.getNom());
//            System.out.println(martine.getVille());
           tx.commit();
       }
       catch(Exception e) {
           System.err.println(e);
           tx.rollback();
       }
       em.close();
       emf.close();
    }
}
