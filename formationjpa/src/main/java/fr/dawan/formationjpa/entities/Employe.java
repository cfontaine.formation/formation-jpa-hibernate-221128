package fr.dawan.formationjpa.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="employes")
public class Employe extends AbstractEntity{

    private static final long serialVersionUID = 1L;

    private String nom;
    
    private double salaire;

    public Employe() {
        super();
    }

    public Employe(String nom, double salaire) {
        super();
        this.nom = nom;
        this.salaire = salaire;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    @Override
    public String toString() {
        return "Employe [nom=" + nom + ", salaire=" + salaire + "]";
    }
    
    
    
    
}
