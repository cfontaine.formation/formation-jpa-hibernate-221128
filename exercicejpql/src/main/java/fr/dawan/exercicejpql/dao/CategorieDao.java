package fr.dawan.exercicejpql.dao;

import javax.persistence.EntityManager;

import fr.dawan.exercicejpql.entities.Categorie;

public class CategorieDao extends AbstractDao<Categorie> {

    private static final long serialVersionUID = 1L;

    public CategorieDao(EntityManager em) {
        super(em, Categorie.class);
    }

}
