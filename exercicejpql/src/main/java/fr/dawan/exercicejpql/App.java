package fr.dawan.exercicejpql;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.dawan.exercicejpql.dao.AuteurDao;
import fr.dawan.exercicejpql.dao.CategorieDao;
import fr.dawan.exercicejpql.dao.LivreDao;
import fr.dawan.exercicejpql.dao.StatLivre;
import fr.dawan.exercicejpql.entities.Auteur;
import fr.dawan.exercicejpql.entities.Categorie;
import fr.dawan.exercicejpql.entities.Livre;

public class App {
    public static void main(String[] args) {
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("bibliotheque");
        EntityManager em=emf.createEntityManager();
        LivreDao livreDao=new LivreDao(em);
        CategorieDao categorieDao= new CategorieDao(em);
        AuteurDao auteurDao=new AuteurDao(em);
        
        System.out.println("Nombre de livre=" + livreDao.count());
        
        List<Livre> lst=livreDao.findByAnnee(1954);
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        lst=livreDao.findByIntervalAnnee(1970, 1979);
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        lst=livreDao.findByAnnees(1932,1950,1992);
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        lst=livreDao.findStartWith("d");
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        System.out.println("-->" +livreDao.countByAnneeSortie(1992));
        
        Categorie c=categorieDao.findbyId(1);
        lst=livreDao.findByCategorie(c);
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        Auteur a=auteurDao.findbyId(1);
        lst=livreDao.findByAuteur(a);
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        lst=livreDao.findMultiAuteur();
        for(Livre l : lst) {
            System.out.println(l);
        }
        
        List<StatLivre> la=livreDao.getStatLivreByAuteur();
        for(StatLivre l : la) {
            System.out.println(l);
        }
        
        List<StatLivre> lc=livreDao.getStatLivreByCategorie();
        for(StatLivre l : lc) {
            System.out.println(l);
        }
        
        em.close();
        emf.close();
    }
}
