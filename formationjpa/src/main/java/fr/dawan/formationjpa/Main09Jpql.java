package fr.dawan.formationjpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.Article;
import fr.dawan.formationjpa.entities.Fournisseur;
import fr.dawan.formationjpa.entities.Marque;

public class Main09Jpql {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();

        // en SQL :SELECT * FROM articles
//        TypedQuery<Article> query=em.createQuery("SELECT a. FROM Article a",Article.class);
//        List<Article> lst=query.getResultList();
//
//        for(Article ar : lst) {
//            System.out.println(ar);
//        }
//       
//        Query query2=em.createQuery("SELECT a.description,a.prix FROM Article a");
//        List<Object[]> lst2=query2.getResultList();
//        for( Object[] tObj : lst2) {
//            System.out.println((String)tObj[0] + " "+ (Double)tObj[1]);
//        }

//        TypedQuery<ArticleSimple> query3=em.createQuery("SELECT new fr.dawan.formationjpa.entities.ArticleSimple(a.description,a.prix) FROM Article a",ArticleSimple.class);
//        List<ArticleSimple> lst3=query3.getResultList();
//        for(ArticleSimple as: lst3) {
//            System.out.println(as);
//        }

        TypedQuery<Article> query4 = em.createQuery("SELECT a FROM Article a WHERE a.prix<30.0", Article.class);
        List<Article> lst4 = query4.getResultList();
        for (Article a1 : lst4) {
            System.out.println(a1);
        }

        TypedQuery<Article> query5 = em.createQuery("SELECT a FROM Article a WHERE a.prix< :prixmax", Article.class);
        query5.setParameter("prixmax", 50.0);
        List<Article> lst5 = query5.getResultList();
        for (Article a1 : lst5) {
            System.out.println(a1);
        }

        TypedQuery<Article> query6 = em.createQuery("SELECT a FROM Article a WHERE a.prix< ?1 AND a.prix>?2",
                Article.class);
        query6.setParameter(1, 50.0);
        query6.setParameter(2, 20.0);
        List<Article> lst6 = query6.getResultList();
        for (Article a1 : lst6) {
            System.out.println(a1);
        }

        List<Article> lst7 = em
                .createQuery("SELECT a FROM Article a WHERE a.prix BETWEEN :prixMin AND :prixMax", Article.class)
                .setParameter("prixMin", 20.0).setParameter("prixMax", 50.0).getResultList();

        for (Article a1 : lst7) {
            System.out.println(a1);
        }

        lst7 = em.createQuery("SELECT a FROM Article a WHERE a.prix IN (40.0,70.0,500.0)", Article.class)
                .getResultList();

        for (Article a1 : lst7) {
            System.out.println(a1);
        }
                                                                        
        lst7 = em.createQuery("SELECT a FROM Article a WHERE a.description LIKE  'C___%'", Article.class)
                .getResultList();

        for (Article a1 : lst7) {
            System.out.println(a1);
        }
        
        lst7 = em.createQuery("SELECT a FROM Article a WHERE a.description LIKE  '%@%' ESCAPE '@'", Article.class)
                .getResultList();

        for (Article a1 : lst7) {
            System.out.println(a1);
        }
        
        List<Marque> lst8=em.createQuery("SELECT m FROM Marque m WHERE m.articles IS EMPTY",Marque.class)
                .getResultList();
        for (Marque m1 : lst8) {
            System.out.println(m1);
        }
        
        long nbArticle = em.createQuery("SELECT COUNT(a) FROM Article a WHERE a.prix>50.0", Long.class)
                .getSingleResult();

        System.out.println(nbArticle);
        
        List<Integer> positions=em.createQuery("SELECT Locate('DDR',a.description) FROM Article a",Integer.class)
                .getResultList();
        for (Integer m1 : positions) {
            System.out.println(m1);
        }
        
        List<String> nbArticleLst=em.createQuery("SELECT CONCAT(m.nom,' : ', SIZE(m.articles)) FROM Marque m ",String.class)
                .getResultList();
        for (String m1 : nbArticleLst) {
            System.out.println(m1);
        }
        
        lst7 = em.createQuery("SELECT a FROM Article a ORDER BY a.prix,a.description DESC", Article.class)
                .getResultList();

        for (Article a1 : lst7) {
            System.out.println(a1);
        }
        
        List<Long> articleLst= em.createQuery("SELECT COUNT(a.marque) FROM Article a GROUP BY a.marque HAVING COUNT(a.marque) >3 ",Long.class)
                .getResultList();
        for (Long i : articleLst) {
            System.out.println(i);
        }
        
        List<String> lstJoin=em.createQuery("SELECT CONCAT(a.description,' ',a.prix, ' ', m.nom) FROM Marque m JOIN m.articles a",String.class)
                .getResultList();
        for(String s: lstJoin) {
            System.out.println(s);
        }
        
        lstJoin=em.createQuery("SELECT CONCAT(count(a) , ' ', m.nom) FROM Marque m LEFT JOIN m.articles a GROUP BY m.nom",String.class)
                .getResultList();
        for(String s: lstJoin) {
            System.out.println(s);
        }
        
//        List<Fournisseur> lstF= em.createQuery("SELECT f FROM Fournisseur f WHERE f IN (SELECT a FROM Article a WHERE a.prix>300.0) ",Fournisseur.class)
//                .getResultList();
//
//                for(Fournisseur s: lstF) {
//                    System.out.println(s);
//                }
                
        List<Article> l1=em.createQuery("SELECT a FROM Article a JOIN FETCH a.fournisseurs",Article.class)
                .getResultList();
//        
//        for(Article ar : l1) {
//            System.out.println(ar);
//            for(Fournisseur f : ar.getFournisseurs()) {
//                System.out.println(f);
//            }
//            System.out.println("-------------");
//        }
        System.out.println("- Fermeture entity manager");
        
        List<Article> l2=em.createNamedQuery("Article.allArticle", Article.class)
                .getResultList();
        for(Article article: l2) {
            System.out.println(article);
        }
        EntityTransaction tx=em.getTransaction();
        try {
            tx.begin();
            // UPDATE
            Query updateQuery=em.createQuery("UPDATE Article a SET a.prix=a.prix+10.0 WHERE a.prix>100.0");
            updateQuery.executeUpdate();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        

        l2=em.createNamedQuery("Article.allArticle", Article.class)
                .getResultList();
        for(Article article: l2) {
            em.refresh(article);
            System.out.println(article);
        }
        try {
            tx.begin();
            // DELETE
            Query deleteQuery=em.createQuery("DELETE FROM Article a WHERE a.prix=19.9");
            deleteQuery.executeUpdate();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
        em.close();   
        
        for(Article ar : l1) {
            System.out.println(ar);
            for(Fournisseur f : ar.getFournisseurs()) {
                System.out.println(f);
            }
            System.out.println("-------------");
        }
        emf.close();
    }
}
