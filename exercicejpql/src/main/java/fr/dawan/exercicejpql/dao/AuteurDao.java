package fr.dawan.exercicejpql.dao;

import javax.persistence.EntityManager;

import fr.dawan.exercicejpql.entities.Auteur;

public class AuteurDao extends AbstractDao<Auteur> {

    private static final long serialVersionUID = 1L;

    public AuteurDao(EntityManager em) {
        super(em, Auteur.class);
    }
}
