package fr.dawan.exercicejpa.entities;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="commandes")
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="numero_commande")
    private long numeroCommande; 
    
    @Column(nullable = false,name="heure_commande")
    private LocalDateTime heureCommande;
    
    @Column(nullable = false,name="heure_livraison")
    private LocalDateTime heureLivraison;

    @ManyToOne
    private Livreur livreur;
    
    @ManyToOne// (fetch=FetchType.LAZY)
    private Client client;
    
    @OneToMany(mappedBy = "commande") //,fetch=FetchType.EAGER)
    private List<CommandPizza> pizzas=new ArrayList<>();
    
    public Commande() {

    }

    public Livreur getLivreur() {
        return livreur;
    }
    
    public void setLivreur(Livreur livreur) {
        this.livreur = livreur;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Commande(LocalDateTime heureCommande, LocalDateTime heureLivraison) {
        this.heureCommande = heureCommande;
        this.heureLivraison = heureLivraison;
    }

    public long getNumeroCommande() {
        return numeroCommande;
    }

    public void setNumeroCommande(long numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public LocalDateTime getHeureCommande() {
        return heureCommande;
    }

    public void setHeureCommande(LocalDateTime heureCommande) {
        this.heureCommande = heureCommande;
    }

    public LocalDateTime getHeureLivraison() {
        return heureLivraison;
    }

    public void setHeureLivraison(LocalDateTime heureLivraison) {
        this.heureLivraison = heureLivraison;
    }

    @Override
    public String toString() {
        return "Commande [numeroCommande=" + numeroCommande + ", heureCommande=" + heureCommande + ", heureLivraison="
                + heureLivraison + "]";
    }
}
