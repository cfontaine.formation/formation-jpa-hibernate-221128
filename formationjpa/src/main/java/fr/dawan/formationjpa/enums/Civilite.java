package fr.dawan.formationjpa.enums;

public enum Civilite {
    MADEMOISELLE,MADAME,MONSIEUR
}
