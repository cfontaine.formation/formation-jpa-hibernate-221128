package fr.dawan.formationjpa.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "compte_bancaires")
// 3 stratégie pour réaliser l'héritage

// - SINGLE_TABLE -> une seule table
// @Inheritance(strategy = InheritanceType.SINGLE_TABLE)
// @DiscriminatorColumn(name = "type_compte") // -> nom de la colonne qui permet de diférencier un compte bancaire et un compte d'épargne 
// @DiscriminatorValue("CB") // -> valeur placée dans la colonne discriminator pour le compte bancaire 

// - TABLE_PER_CLASS -> une table pour le compte bancaire et une pour compte d'épargne
// @Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

// - JOINED -> Une table qui contient les données de compte bancaire et une table lié par un jointure
//             qui contient ce qui est spécifique (taux) au compte d'épargne
@Inheritance(strategy = InheritanceType.JOINED)

public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String titulaire;

    private double solde;

    private String iban;

    public CompteBancaire() {

    }

    public CompteBancaire(String titulaire, double solde, String iban) {
        this.titulaire = titulaire;
        this.solde = solde;
        this.iban = iban;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @Override
    public String toString() {
        return "CompteBancaire [id=" + id + ", titulaire=" + titulaire + ", solde=" + solde + ", iban=" + iban + "]";
    }

}
