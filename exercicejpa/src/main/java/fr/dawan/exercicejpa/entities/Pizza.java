package fr.dawan.exercicejpa.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import fr.dawan.exercicejpa.enums.BaseEnum;

@Entity
@Table(name="pizzas")
public class Pizza implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(nullable = false,length=50)
    private String nom;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, length=10)
    private BaseEnum base=BaseEnum.ROUGE;
    
    @Column(nullable = false)
    private double prix;
    
    @Lob
    private byte[] photo;
    
    @ManyToMany(mappedBy = "pizzas")
    private List<Ingredient> ingredients=new ArrayList<>();

    @OneToMany(mappedBy="pizza")
    private List<CommandPizza> commandes=new ArrayList<>();
    
    
    public Pizza() {

    }

    public Pizza(String nom, BaseEnum base, double prix, byte[] photo) {
        this.nom = nom;
        this.base = base;
        this.prix = prix;
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public BaseEnum getBase() {
        return base;
    }

    public void setBase(BaseEnum base) {
        this.base = base;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Pizza [id=" + id + ", nom=" + nom + ", base=" + base + ", prix=" + prix + ", photo="
                + Arrays.toString(photo) + "]";
    }
}
