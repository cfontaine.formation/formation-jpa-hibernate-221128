package fr.dawan.exercicejpql.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "nations")
public class Nation extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, length = 100)
    private String nom;

    public Nation() {

    }

    public Nation(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Nation [" + super.toString() + ", nom =" + nom + "]";
    }

}
