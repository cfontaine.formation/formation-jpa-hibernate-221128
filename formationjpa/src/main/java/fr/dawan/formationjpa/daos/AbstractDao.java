package fr.dawan.formationjpa.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.AbstractEntity;

public abstract class AbstractDao<T extends AbstractEntity> {

    protected EntityManager em;

    private Class<T> classEntity;

    public AbstractDao(EntityManager em, Class<T> classEntity) {
        this.em = em;
        this.classEntity = classEntity;
    }

    public void saveOrUpdate(T elm) throws Exception{
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            if (elm.getId() == 0) {
                em.persist(elm);
            } else {
                em.merge(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    public void remove(T elm) throws Exception{
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            if (elm.getId() != 0) {
                em.remove(elm);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    public T findbyId(long id){
        return em.find(classEntity, id);
    }

    public List<T> findAll() {
        TypedQuery<T> query = em.createQuery("SELECT e FROM " + classEntity.getName() + " e", classEntity);
        return query.getResultList();
    }
}
