package fr.dawan.exercicejpa.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

@Embeddable
public class CommandePizzaId implements Serializable {

    private static final long serialVersionUID = 1L;

    private long commandeId;
    
    private long pizzaId; 
    
    public CommandePizzaId() {

    }

    public CommandePizzaId(long commandeId, long pizzaId) {
        this.commandeId = commandeId;
        this.pizzaId = pizzaId;
    }

    public long getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(long commandeId) {
        this.commandeId = commandeId;
    }

    public long getPizzaId() {
        return pizzaId;
    }

    public void setPizzaId(long pizzaId) {
        this.pizzaId = pizzaId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(commandeId, pizzaId);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CommandePizzaId other = (CommandePizzaId) obj;
        return commandeId == other.commandeId && pizzaId == other.pizzaId;
    }

}
