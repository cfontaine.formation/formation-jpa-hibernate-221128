package fr.dawan.formationjpa;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.Adresse;
import fr.dawan.formationjpa.entities.Personne;
import fr.dawan.formationjpa.enums.Civilite;

public class Main10Cache {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Personne p1 = new Personne("John", "Doe", LocalDate.of(1995, 10, 20), "jd@dawan.com");
       

        System.out.println(p1);
        try {
            tx.begin(); // -> démarrer la transaction
            // Peristence de l'objet p1 dans la bdd
          //  em.persist(p1);
            long id=5L;//p1.getId();
            Personne p2 =em.find(Personne.class, id);
            System.out.println(p2);
            Personne p3 =em.find(Personne.class, id);
            System.out.println(p3);
            tx.commit(); // transaction -> valider
        } catch (Exception e) {
            tx.rollback(); // transaction -> annuler
        }
        em.close();
        emf.close();
    }
}
