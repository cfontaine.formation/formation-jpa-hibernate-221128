package fr.dawan.formationjpa.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

import fr.dawan.formationjpa.enums.Emballage;
import fr.dawan.formationjpa.listeners.CustomListener;

@Entity
@Table(name = "articles")
@NamedQueries({
    @NamedQuery(
            name="Article.allArticle",
            query="SELECT a FROM Article a"
            )
})

@EntityListeners(CustomListener.class)
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version; 

    private String description;

    private double prix;

    @Enumerated(EnumType.STRING)
    private Emballage emballage;
    
    @ManyToOne
    @JoinColumn(name = "id_marque")
    private Marque marque;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "article2fournisseur"
    ,joinColumns = @JoinColumn(name="fk_article")
    ,inverseJoinColumns = @JoinColumn(name="fk_founisseur"))
    private Set<Fournisseur> fournisseurs=new HashSet<>();

    public Article() {

    }

    public Article(String description, double prix, Emballage emballage) {
        super();
        this.description = description;
        this.prix = prix;
        this.emballage = emballage;
    }
    
    public Set<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(Set<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Emballage getEmballage() {
        return emballage;
    }

    public void setEmballage(Emballage emballage) {
        this.emballage = emballage;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Article [id=" + id + ", description=" + description + ", prix=" + prix + ", emballage=" + emballage
                + "]";
    }

}
