package fr.dawan.formationjpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.CompteBancaire;
import fr.dawan.formationjpa.entities.CompteEpargne;

public class Main02Heritage {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        CompteBancaire cb1 = new CompteBancaire("John Doe", 500.0, "fr-5962-5647368");
        CompteBancaire cb2 = new CompteBancaire("Jane Doe", 50.0, "fr-5962-5654568");
        CompteEpargne ce1 = new CompteEpargne("Jane Doe", 90.0, "fr-5962-564562", 0.010);
        CompteEpargne ce2 = new CompteEpargne("Alan Smithee", 150.0, "fr-5962-5643562", 0.015);

        try {
            tx.begin();
            em.persist(cb1);
            em.persist(cb2);
            em.persist(ce1);
            em.persist(ce2);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        emf.close();
    }
}
