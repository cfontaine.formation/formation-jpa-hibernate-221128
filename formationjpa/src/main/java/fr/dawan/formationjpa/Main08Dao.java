package fr.dawan.formationjpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.daos.EmployeDao;
import fr.dawan.formationjpa.entities.Employe;

public class Main08Dao {

    public static void main(String[] args) {
        EntityManagerFactory emf=Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em=emf.createEntityManager();
        EmployeDao dao=new EmployeDao(em);
        Employe emp1=new Employe("John Doe",1900.0);
        Employe emp2=new Employe("Jane Doe",2500.0);
        Employe emp3=new Employe("Alan Smithee",1500.0);
        
        try {
            dao.saveOrUpdate(emp1);
            dao.saveOrUpdate(emp2);
            dao.saveOrUpdate(emp3);
            emp1.setSalaire(2000.0);
            
            dao.saveOrUpdate(emp1);
            
            Employe e1=dao.findbyId(2L);
            System.out.println(e1);
            
            List<Employe> lst=dao.findAll();
            for(Employe emp : lst) {
                System.out.println(emp);
            }

            dao.remove(emp3);
            lst=dao.findAll();
            for(Employe emp : lst) {
                System.out.println(emp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }      
        finally {
            em.close();
            emf.close();
        }
    }

}
