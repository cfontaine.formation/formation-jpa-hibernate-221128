package fr.dawan.exercicejpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.exercicejpa.entities.Ingredient;

public class App {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicejpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Ingredient i1=new Ingredient("tomate");
        Ingredient i2=new Ingredient("Jambon");
        try {
            tx.begin();
            em.persist(i1);
            em.persist(i2);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        emf.close();
    }
}
