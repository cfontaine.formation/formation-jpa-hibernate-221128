package fr.dawan.exercicejpql.entities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "auteurs")
public class Auteur extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    @Column(nullable = false, length = 50)
    private String nom;

    @Column(nullable = false, length = 50)
    private String prenom;

    @Column(nullable = false, name = "naissance")
    private LocalDate dateNaissance;

    @Column(name = "deces")
    private LocalDate dateDeces;
    
    @ManyToOne
    private Nation nation;
    
    @ManyToMany
    private List<Livre> livres=new ArrayList<>();
    

    public Auteur() {

    }

    public Auteur(String nom, String prenom, LocalDate dateNaissance, LocalDate dateDeces) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.dateDeces = dateDeces;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public LocalDate getDateDeces() {
        return dateDeces;
    }

    public void setDateDeces(LocalDate dateDeces) {
        this.dateDeces = dateDeces;
    }

    public Nation getNation() {
        return nation;
    }

    public void setNation(Nation nation) {
        this.nation = nation;
    }

    public List<Livre> getLivres() {
        return livres;
    }

    public void setLivres(List<Livre> livres) {
        this.livres = livres;
    }

    @Override
    public String toString() {
        return "Auteur [" + super.toString() + ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance="
                + dateNaissance + ", dateDeces=" + dateDeces + "]";
    }
}
