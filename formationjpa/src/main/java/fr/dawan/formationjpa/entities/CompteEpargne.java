package fr.dawan.formationjpa.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "compte_epargne")
//@DiscriminatorValue("CE") // -> valeur placée dans la colonne discriminator pour le compte d'épargne
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;

    private double taux = 0.05;

    public CompteEpargne() {
        super();

    }

    public CompteEpargne(String titulaire, double solde, String iban, double taux) {
        super(titulaire, solde, iban);
        this.taux = taux;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return "CompteEpargne [taux=" + taux + "]";
    }

}
