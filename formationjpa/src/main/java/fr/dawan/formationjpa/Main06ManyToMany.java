package fr.dawan.formationjpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.Article;
import fr.dawan.formationjpa.entities.Fournisseur;
import fr.dawan.formationjpa.entities.Marque;
import fr.dawan.formationjpa.enums.Emballage;

public class Main06ManyToMany {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Article a1=new Article("Tv 4K",600.0,Emballage.CARTON);
        Article a2=new Article("Souris",30.0,Emballage.PLASTIQUE);
        Article a3=new Article("Stylo bille",3.50,Emballage.SANS);
        Marque m1=new Marque("Marque A");
        Marque m2=new Marque("Marque B");
//        a1.setMarque(m1);
//        a2.setMarque(m1);
//        a3.setMarque(m2);
//        m1.getArticle().add(a1);
//        m1.getArticle().add(a2);
//        m2.getArticle().add(a3);
        m1.ajoutArticle(a1);
        m1.ajoutArticle(a2);
        m2.ajoutArticle(a3);

        Fournisseur f1=new Fournisseur("Fournisseur 1");
        Fournisseur f2=new Fournisseur("Fournisseur 2");
        a1.getFournisseurs().add(f1);
        a1.getFournisseurs().add(f2);
        a2.getFournisseurs().add(f2);
        a3.getFournisseurs().add(f1);
        
        f1.getArticles().add(a1);
        f1.getArticles().add(a3);
        
        f2.getArticles().add(a1);
        f2.getArticles().add(a2);
        
        
        try {
            tx.begin();
            em.persist(m1);
            em.persist(m2);
            em.persist(a1);
            em.persist(a2);
            em.persist(a3);
            em.persist(f1);
            em.persist(f2);
            a3.setPrix(5.0);
            tx.commit(); 
        } catch (Exception e) {
            tx.rollback(); 
        }
        em.close();
        emf.close();
    }
}
