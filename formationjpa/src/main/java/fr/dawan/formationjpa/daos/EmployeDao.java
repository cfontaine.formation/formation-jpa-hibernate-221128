package fr.dawan.formationjpa.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import fr.dawan.formationjpa.entities.Employe;

public class EmployeDao extends AbstractDao<Employe> {

    public EmployeDao(EntityManager em) {
        super(em, Employe.class);
    }

    public List<Employe> findBySalaireMin(double salaireMin){
        TypedQuery<Employe> query=em.createQuery("SELECT e FROM Employe e WHERE e.salaire > :min ORDER BY e.salaire ",Employe.class);
        query.setParameter("min", salaireMin);
        query.setFirstResult(0).setMaxResults(3); // LIMIT 3 en SQL
        return query.getResultList();
    }
    
}
