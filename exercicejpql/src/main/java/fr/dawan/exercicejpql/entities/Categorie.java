package fr.dawan.exercicejpql.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "categories")
public class Categorie extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String nom;

    @OneToMany(mappedBy = "categorie")
    private List<Livre> livres;
    
    public Categorie() {
        super();
    }

    public Categorie(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Categorie [" + super.toString() + ", nom=" + nom + "]";
    }

}
