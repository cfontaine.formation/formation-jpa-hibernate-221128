package fr.dawan.formationjpa.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Version;

import fr.dawan.formationjpa.enums.Civilite;

@Entity
@Table(name = "personnes")
//@TableGenerator(name = "personne_generator")
//@SequenceGenerator(name= "personne_sequence")
public class Personne implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @GeneratedValue(strategy = GenerationType.AUTO);
    // @GeneratedValue(strategy=GenerationType.TABLE, generator =
    // "personne_generator")
    // @GeneratedValue(strategy=GenerationType.SEQUENCE, generator =
    // "personne_sequence")
    private long id;

    @Version
    private int version;

    @Column(length = 50)
    private String prenom;

    @Column(nullable = false, length = 50)
    private String nom;

    @Column(name = "date_naissance", nullable = false)
    private LocalDate dateNaissance;

    @Column(nullable = true, length = 1024)
    private String email;

    // @Transient
    private transient String nePasPersister;

    @Column(length = 20)
    @Enumerated(EnumType.STRING)
    private Civilite civilite;

    @Lob
    private byte[] photo;

    @Embedded
    private Adresse adressePerso;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "ville", column = @Column(name = "ville_pro")),
            @AttributeOverride(name = "rue", column = @Column(name = "rue_pro")),
            @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_pro")) })
    private Adresse adressePro;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "numero_telephones", joinColumns = @JoinColumn(name = "id_personne"))
    // @MapKeyColumn(name = "numTelephone")
    @Column(name = "num_telephone")
    private List<String> numTelephones = new ArrayList<>();

    public Personne() {

    }

    public Personne(String prenom, String nom, LocalDate dateNaissance, String email) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.email = email;
    }

    public List<String> getNumTelephones() {
        return numTelephones;
    }

    public void setNumTelephones(List<String> numTelephones) {
        this.numTelephones = numTelephones;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNePasPersister() {
        return nePasPersister;
    }

    public void setNePasPersister(String nePasPersister) {
        this.nePasPersister = nePasPersister;
    }

    public Civilite getCivilite() {
        return civilite;
    }

    public void setCivilite(Civilite civilite) {
        this.civilite = civilite;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Adresse getAdressePerso() {
        return adressePerso;
    }

    public void setAdressePerso(Adresse adressePerso) {
        this.adressePerso = adressePerso;
    }

    public Adresse getAdressePro() {
        return adressePro;
    }

    public void setAdressePro(Adresse adressePro) {
        this.adressePro = adressePro;
    }

    @Override
    public String toString() {
        return "Personne [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", dateNaissance=" + dateNaissance
                + ", email=" + email + "]";
    }
}
