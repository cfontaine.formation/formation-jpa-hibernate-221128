package fr.dawan.formationjpa;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.Adresse;
import fr.dawan.formationjpa.entities.Personne;

public class Main03CycleDeVie {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Personne p1 = new Personne("John", "Doe", LocalDate.of(1995, 10, 20), "jd@dawan.com");
        Adresse adr = new Adresse("46 rue des canonniers", "Lille", "59000");
        System.out.println(p1);
        try {
            tx.begin();
            System.out.println(p1);
            em.persist(p1);

           em.detach(p1);
//           
          p1=em.merge(p1);
            System.out.println(p1);
            
            p1.setAdressePerso(adr);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();

//        EntityManager em2 = emf.createEntityManager();
//        tx = em2.getTransaction();
//        try {
//            tx.begin();
//            p1=em2.merge(p1);
//            em2.remove(p1);
//            tx.commit();
//        } catch (Exception e) {
//            tx.rollback();
//        }
//        em2.close();
        
//        EntityManager em2 = emf.createEntityManager();
//        tx = em2.getTransaction();
//        try {
//            tx.begin();
//            p1=em2.find(Personne.class,1);
//            em2.remove(p1);
//            tx.commit();
//        } catch (Exception e) {
//            tx.rollback();
//        }
//        em2.close();
        emf.close();
    }
}
