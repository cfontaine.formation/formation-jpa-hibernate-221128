package fr.dawan.formationjpa;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.formationjpa.entities.Adresse;
import fr.dawan.formationjpa.entities.Personne;
import fr.dawan.formationjpa.enums.Civilite;

public class App {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("formationjpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Personne p1 = new Personne("John", "Doe", LocalDate.of(1995, 10, 20), "jd@dawan.com");
        // p1.setId(1);
        p1.setCivilite(Civilite.MONSIEUR);
        p1.setDateNaissance(LocalDate.of(1995, 3, 4));
        p1.setAdressePerso(new Adresse("rue esquermoise", "Lille", "59000"));
        p1.setAdressePro(new Adresse("46, rue des cannoniers", "Lille", "59000"));

        System.out.println(p1);
        try {
            tx.begin(); // -> démarrer la transaction
            // Peristence de l'objet p1 dans la bdd
            em.persist(p1);
            // Modification de l'état de p1, elle est persitée dans la bdd
            p1.setEmail("jd@jehann.com");
            // Suppression de l'objet p1 dans la bdd
            em.remove(p1);
            tx.commit(); // transaction -> valider
        } catch (Exception e) {
            tx.rollback(); // transaction -> annuler
        }
        em.close();
        emf.close();
    }
}
