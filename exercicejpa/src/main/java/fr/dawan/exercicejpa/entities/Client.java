package fr.dawan.exercicejpa.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "clients")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long numeroClient;

    @Column(nullable = false, length = 60)
    private String nom;
    
    @Column(nullable = false)
    private String adresse;

    @OneToMany(mappedBy = "client")
    private List<Commande> commandes;
    
    public Client() {
        super();
    }

    public Client(long numeroClient, String nom, String adresse) {
        super();
        this.numeroClient = numeroClient;
        this.nom = nom;
        this.adresse = adresse;
    }

    public long getNumeroClient() {
        return numeroClient;
    }

    public void setNumeroClient(long numeroClient) {
        this.numeroClient = numeroClient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    @Override
    public String toString() {
        return "Client [numeroClient=" + numeroClient + ", nom=" + nom + ", adresse=" + adresse + "]";
    }
    
    
}
