package fr.dawan.exercicejpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import fr.dawan.exercicejpa.entities.qcm.Qcm;
import fr.dawan.exercicejpa.entities.qcm.QcmQuestion;
import fr.dawan.exercicejpa.entities.qcm.QcmReponse;

public class MainQcm {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("exercicejpa");
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        
        Qcm qcm1=new Qcm("Qcm Java");
        QcmQuestion question1=new QcmQuestion("Donner les types primaire à virgule flottante", true, 1);
        QcmReponse rep1=new QcmReponse("int", false);
        QcmReponse rep2=new QcmReponse("double", true);
        QcmReponse rep3=new QcmReponse("decimal", false);
        QcmReponse rep4=new QcmReponse("char", false);
        QcmReponse rep5=new QcmReponse("float", true);

        rep1.setQuestion(question1);
        rep2.setQuestion(question1);
        rep3.setQuestion(question1);
        rep4.setQuestion(question1);
        rep5.setQuestion(question1);
        
        question1.setQcm(qcm1);
        try {
            tx.begin();
            em.persist(qcm1);
            em.persist(question1);
            em.persist(rep1);
            em.persist(rep2);
            em.persist(rep3);
            em.persist(rep4);
            em.persist(rep5);
            
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        em.close();
        emf.close();
    }
}
